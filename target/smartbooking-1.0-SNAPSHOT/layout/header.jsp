<%-- 
    Document   : header
    Created on : Sep 29, 2019, 10:00:10 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <meta name="google-signin-client_id" content="449822258828-pj0q6d5f26vh1sc3upk3176ion7u0qc5.apps.googleusercontent.com">
        <title>Smart Booking</title>
        <!-- GLOBAL MAINLY STYLES-->
        <script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
        <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
        <link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />
        <link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <link href="./assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
        <!-- THEME STYLES-->
        <link href="assets/css/main.min.css" rel="stylesheet" />


        <link href="./assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />

        <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <link href="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />


        <link href="./assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
        <link href="./assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
        <link href="./assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css" rel="stylesheet" />

        <link href="./assets/vendors/multiselect/css/multi-select.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="./assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />



        <link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />
        <link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />
        <link href="./assets/vendors/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" />

        <link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/css/pages/timeline.css" rel="stylesheet" />
        <link href="./assets/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />
        <link href="./assets/vendors/fullcalendar/dist/fullcalendar.print.min.css" rel="stylesheet" media="print" />


        <!-- THEME STYLES-->

        <!-- PAGE LEVEL STYLES-->

        <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
    </head>

    <script>
        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');

                $.ajax({
                    url: "SessionLogout",
                    success: function (result) {
                        //alert(result);
                        //window.location.replace(result);
                        //$('html').empty().load(href).hide().fadeIn(300);
                        window.location.href = 'index.jsp';
                    }
                });
            });
        }

        function onLoad() {
            gapi.load('auth2', function () {
                gapi.auth2.init();
            });
        }
    </script>
</head>