<%-- 
    Document   : sidebar
    Created on : Sep 29, 2019, 10:06:03 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<nav class="page-sidebar">
    <ul class="side-menu metismenu scroller">
        <li class="active">
            <a href="Login"><i class="sidebar-item-icon ti-home"></i>
                <span class="nav-label">Dashboards</span></a>

        </li>
        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-paint-roller"></i>
                <span class="nav-label">Configuration</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <li>
                    <a href="conf_car.jsp">Car</a>
                </li>
                <li>
                    <a href="conf_driver.jsp">Driver</a>
                </li>
                
            </ul>
        </li>

        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-anchor"></i>
                <span class="nav-label">Booking</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <li>
                    <a href="book_list.jsp">List of Booking</a>
                </li><li>
                    <a href="book_calendar.jsp">Booking Calendar</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-anchor"></i>
                <span class="nav-label">Insight</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <li>
                    <a href="book_list.jsp">Driver</a>
                </li><li>
                    <a href="book_calendar.jsp">Car</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>