<%-- 
    Document   : index
    Created on : Sep 29, 2019, 9:54:45 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="google-signin-client_id" content="449822258828-pj0q6d5f26vh1sc3upk3176ion7u0qc5.apps.googleusercontent.com">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <title>Adminca bootstrap 4 &amp; angular 5 admin template, Шаблон админки | Login v3</title>
        <!-- GLOBAL MAINLY STYLES-->
        <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
        <link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />
        <link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <!-- THEME STYLES-->
        <link href="assets/css/main.min.css" rel="stylesheet" />
        <!-- PAGE LEVEL STYLES-->
        <style>
            body {
                background-color: #4a5ab9;
            }

            .login-content {
                max-width: 900px;
                margin: 100px auto 50px;
            }

            .auth-head-icon {
                position: relative;
                height: 60px;
                width: 60px;
                display: inline-flex;
                align-items: center;
                justify-content: center;
                font-size: 30px;
                background-color: #fff;
                color: #5c6bc0;
                box-shadow: 0 5px 20px #d6dee4;
                border-radius: 50%;
                transform: translateY(-50%);
                z-index: 2;
            }
        </style>
        <script type="text/javascript" charset="utf-8">
            function onSignIn(googleUser) {
                var profile = googleUser.getBasicProfile();
                console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                console.log('Name: ' + profile.getName());
                console.log('Image URL: ' + profile.getImageUrl());
                console.log('Email: ' + profile.getEmail()); // This isnull if the 'email' scope is not present.

                var auth2 = gapi.auth2.init();
                if (auth2.isSignedIn.get()) {
                    console.log('signedin');
                    window.location.href = 'SessionLogin?userID=' + profile.getId() + '&email=' + profile.getEmail() + '&name=' + profile.getName() + '&imageurl=' + profile.getImageUrl();


                }
            }


            function onFailure(error) {
                console.log(error);
            }
            function renderButton() {
                gapi.signin2.render('my-signin2', {
                    'scope': 'profile email',
                    'width': 360,
                    'height': 50,
                    'longtitle': true,
                    'theme': 'dark',
                    'onsuccess': onSignIn,
                    'onfailure': onFailure
                });
            }
        </script>
        <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
    </head>

    <body>
        <div class="row login-content">
            <div class="col-lg-6 col-sm-12 bg-white">
                <div class="text-center">
                    <span class="auth-head-icon"><img src="./assets/img/logolcsb3.png" width="85%"></span>
                </div>
                <div class="ibox m-0" style="box-shadow: none;">
                    <form class="ibox-body" id="login-form" action="javascript:;" method="POST">
                        <h4 class="font-strong text-center mb-5">LOG IN</h4>
                        <div id="my-signin2"></div>
                        
                        
                    </form>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 d-inline-flex align-items-center text-white py-4 px-5">
                <div>
                    <div class="h2 mb-4">Car Booking Management System</div>
                    <!--<div class="mb-4 pt-3">
                        <button class="btn btn-outline btn-icon-only btn-circle mr-3"><i class="fa fa-facebook"></i></button>
                        <button class="btn btn-outline btn-icon-only btn-circle mr-3"><i class="fa fa-twitter"></i></button>
                        <button class="btn btn-outline btn-icon-only btn-circle mr-3"><i class="fa fa-pinterest-p"></i></button>
                    </div>-->
                    <p>Book your car with simple steps.</p>
                    <div class="flexbox-b mb-3"><i class="ti-check mr-3 text-success"></i>Easy to manage.</div>
                    <div class="flexbox-b mb-3"><i class="ti-check mr-3 text-success"></i>Check your booking status online.</div>
                    <div class="flexbox-b mb-5"><i class="ti-check mr-3 text-success"></i>Trace previous record easily.</div>
                    <button class="btn btn-outline btn-rounded btn-fix">Contact Administrator</button>
                </div>
            </div>
        </div>
        <!-- BEGIN PAGA BACKDROPS-->
        <div class="sidenav-backdrop backdrop"></div>
        <div class="preloader-backdrop">
            <div class="page-preloader">Loading</div>
        </div>
        <!-- CORE PLUGINS-->
        <script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
        <script src="./assets/vendors/popper.js/dist/umd/popper.min.js"></script>
        <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js"></script>
        <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="./assets/vendors/jquery-idletimer/dist/idle-timer.min.js"></script>
        <script src="./assets/vendors/toastr/toastr.min.js"></script>
        <script src="./assets/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="./assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <!-- PAGE LEVEL PLUGINS-->
        <!-- CORE SCRIPTS-->
        <script src="assets/js/app.min.js"></script>
        <!-- PAGE LEVEL SCRIPTS-->
        <script>
            $(function () {
                $('#login-form').validate({
                    errorClass: "help-block",
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true
                        }
                    },
                    highlight: function (e) {
                        $(e).closest(".form-group").addClass("has-error")
                    },
                    unhighlight: function (e) {
                        $(e).closest(".form-group").removeClass("has-error")
                    },
                });
            });
        </script>
    </body>

</html>