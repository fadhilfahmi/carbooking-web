<%-- 
    Document   : conf_car
    Created on : Sep 29, 2019, 10:49:58 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.model.Car"%>
<%@page import="com.lcsb.smartbooking.dao.CarDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");


%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#addnew').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'conf_driver_add.jsp');

            return false;
        });

        $('#viewmodal').click(function (e) {

            e.preventDefault();
            $.ajax({
                async: false,
                url: "PathController?process=addmodaldriver",
                success: function (result) {
                    //alert(result);
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')

            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodaldriver&driverID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_driver_edit.jsp?id=' + id);

            return false;
        });

        $('#modalhere').on('click', '#deleteButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletedriver&carID=" + id,
                    success: function (result) {
                       //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'conf_driver.jsp');
                    }
                });
                
            });


            return false;
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">

            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Driver Information</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="main.jsp"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Configuration</li>
                    <li class="breadcrumb-item">Driver Information</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">
                <div class="ibox">
                    <div class="ibox-body">
                        <h5 class="font-strong mb-4">List of Driver</h5>

                        <div class="flexbox mb-4">

                            <div class="flexbox mb-4">


                                <button id="addnew" class="btn btn-primary btn-air mr-4">Add Driver</button>
                                <label class="mb-0 mr-2">Type:</label>
                                <select class="selectpicker show-tick form-control" id="type-filter" title="Please select" data-style="btn-solid" data-width="150px">
                                    <option value="">All</option>
                                    <option>Shipped</option>
                                    <option>Completed</option>
                                    <option>Pending</option>
                                    <option>Canceled</option>
                                </select>
                            </div>
                            <div class="input-group-icon input-group-icon-left mr-3">
                                <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                                <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                            </div>
                        </div>
                        <div class="table-responsive row">
                            <table class="table table-bordered table-hover" id="datatable">
                                <thead class="thead-default thead-lg">
                                    <tr>
                                        <th>#</th>
                                        <th>Driver ID</th>
                                        <th>Name</th>
                                        <th>Staff ID</th>
                                        <th>Status</th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%List<Driver> listAll = (List<Driver>) DriverDAO.getAllDriver(log);
                                        int i = 0;

                                        for (Driver j : listAll) {
                                            i++;

                                    %>
                                    <tr id="<%= j.getDriverID()%>">
                                        <td><%= i%></td>
                                        <td>
                                            <a href="javascript:;"><%= j.getDriverID()%></a>
                                        </td>
                                        <td><%= j.getDriverName()%></td>

                                        <td><%= j.getStaffID()%></td><td>
                                            <span class="badge badge-<%= CarDAO.getBadgeColor(j.getStatus())%> badge-pill"><%= j.getStatus()%></span>
                                        </td>
                                        <td>
                                            <a class="text-muted font-16" href="javascript:;"><i class="ti-trash"></i></a>
                                        </td>
                                    </tr>

                                    <% }%>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>

        </div>



        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>
<script>
    $(function () {
        $('#datatable').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
    });
</script>
</html>
