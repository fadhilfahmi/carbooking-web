package com.lcsb.smartbooking.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-22T09:46:27")
@StaticMetamodel(BookingDestinationTemp.class)
public class BookingDestinationTemp_ { 

    public static volatile SingularAttribute<BookingDestinationTemp, String> enddate;
    public static volatile SingularAttribute<BookingDestinationTemp, String> destcode;
    public static volatile SingularAttribute<BookingDestinationTemp, String> endtime;
    public static volatile SingularAttribute<BookingDestinationTemp, Integer> id;
    public static volatile SingularAttribute<BookingDestinationTemp, String> sessionid;
    public static volatile SingularAttribute<BookingDestinationTemp, String> starttime;
    public static volatile SingularAttribute<BookingDestinationTemp, String> destdescp;
    public static volatile SingularAttribute<BookingDestinationTemp, String> startdate;
    public static volatile SingularAttribute<BookingDestinationTemp, String> desttype;
    public static volatile SingularAttribute<BookingDestinationTemp, String> userID;

}