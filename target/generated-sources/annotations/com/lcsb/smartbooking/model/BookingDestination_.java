package com.lcsb.smartbooking.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-22T09:46:27")
@StaticMetamodel(BookingDestination.class)
public class BookingDestination_ { 

    public static volatile SingularAttribute<BookingDestination, Integer> daycount;
    public static volatile SingularAttribute<BookingDestination, String> enddate;
    public static volatile SingularAttribute<BookingDestination, String> destcode;
    public static volatile SingularAttribute<BookingDestination, String> endtime;
    public static volatile SingularAttribute<BookingDestination, Integer> id;
    public static volatile SingularAttribute<BookingDestination, String> starttime;
    public static volatile SingularAttribute<BookingDestination, String> destdescp;
    public static volatile SingularAttribute<BookingDestination, String> startdate;
    public static volatile SingularAttribute<BookingDestination, String> desttype;
    public static volatile SingularAttribute<BookingDestination, String> userID;
    public static volatile SingularAttribute<BookingDestination, String> bookID;

}