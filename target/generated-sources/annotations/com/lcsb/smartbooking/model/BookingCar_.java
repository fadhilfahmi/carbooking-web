package com.lcsb.smartbooking.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-22T09:46:27")
@StaticMetamodel(BookingCar.class)
public class BookingCar_ { 

    public static volatile SingularAttribute<BookingCar, String> reason;
    public static volatile SingularAttribute<BookingCar, String> driverID;
    public static volatile SingularAttribute<BookingCar, Integer> destID;
    public static volatile SingularAttribute<BookingCar, String> drivetype;
    public static volatile SingularAttribute<BookingCar, Integer> id;
    public static volatile SingularAttribute<BookingCar, String> appdate;
    public static volatile SingularAttribute<BookingCar, String> bookID;
    public static volatile SingularAttribute<BookingCar, String> carID;
    public static volatile SingularAttribute<BookingCar, String> status;

}