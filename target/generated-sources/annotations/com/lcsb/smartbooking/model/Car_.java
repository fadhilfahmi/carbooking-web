package com.lcsb.smartbooking.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-22T09:46:27")
@StaticMetamodel(Car.class)
public class Car_ { 

    public static volatile SingularAttribute<Car, String> driverID;
    public static volatile SingularAttribute<Car, String> plateNo;
    public static volatile SingularAttribute<Car, String> remark;
    public static volatile SingularAttribute<Car, String> carID;
    public static volatile SingularAttribute<Car, String> descp;
    public static volatile SingularAttribute<Car, String> status;

}