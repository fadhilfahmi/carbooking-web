package com.lcsb.smartbooking.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-22T09:46:27")
@StaticMetamodel(BookingPassenger.class)
public class BookingPassenger_ { 

    public static volatile SingularAttribute<BookingPassenger, String> compID;
    public static volatile SingularAttribute<BookingPassenger, Integer> id;
    public static volatile SingularAttribute<BookingPassenger, String> bookID;

}