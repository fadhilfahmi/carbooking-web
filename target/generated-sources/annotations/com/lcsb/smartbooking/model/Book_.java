package com.lcsb.smartbooking.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-22T09:46:27")
@StaticMetamodel(Book.class)
public class Book_ { 

    public static volatile SingularAttribute<Book, Integer> daycount;
    public static volatile SingularAttribute<Book, String> reason;
    public static volatile SingularAttribute<Book, String> dateapply;
    public static volatile SingularAttribute<Book, String> staffname;
    public static volatile SingularAttribute<Book, String> bookID;
    public static volatile SingularAttribute<Book, String> appname;
    public static volatile SingularAttribute<Book, String> drivetype;
    public static volatile SingularAttribute<Book, String> appID;
    public static volatile SingularAttribute<Book, String> appemail;
    public static volatile SingularAttribute<Book, String> appdate;
    public static volatile SingularAttribute<Book, String> staffID;
    public static volatile SingularAttribute<Book, String> email;
    public static volatile SingularAttribute<Book, String> status;

}