/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.smartbooking.dao;

import com.lcsb.smartbooking.dao.LoginDAO;
import com.lcsb.smartbooking.model.LoginProfile;
import com.lcsb.smartbooking.model.Members;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();

            try {
                HttpSession session = request.getSession();
                SessionConnection sessionConnection = (SessionConnection) session.getAttribute("sessionconnection");

                Connection connection = null;
                if (sessionConnection != null) {
                    connection = sessionConnection.getConnection();
                    Logger.getLogger(Login.class.getName()).log(Level.INFO, "-------connection----" + connection);
                }
                String urlsend = "";

                LoginProfile login = (LoginProfile) LoginDAO.setLogin(connection);

                login.setEmail((String) session.getAttribute("sessionemail"));
                login.setFullname((String) session.getAttribute("sessionname"));
                login.setImageURL((String) session.getAttribute("sessionimageurl"));
                
                login.setUserID((String) session.getAttribute("sessionuserid"));

                

                if (LoginDAO.checkUserUsingEmail((String) session.getAttribute("sessionemail"), connection)) {

                    LoginDAO.updateMemberWithGoogleCredential(connection, (String) session.getAttribute("sessionemail"), (String) session.getAttribute("sessionuserid"), (String) session.getAttribute("sessionname"), (String) session.getAttribute("sessionimageurl"));

                } else {

                    Members member = new Members();
                    member.setCompID(login.getUserID());
                    member.setEmail(login.getEmail());
                    member.setImageURL(login.getImageURL());
                    member.setLevel(0);
                    member.setMemberName(login.getFullname());
                    member.setPosition("n/a");

                    LoginDAO.saveMemberInfo(login, member);

                }
                
                login.setAccessLevel(LoginDAO.userInfo(connection, (String) session.getAttribute("sessionemail")).getLevel());
                session.setAttribute("login_detail", login);
                session.removeAttribute("login_error");

                urlsend = "/main.jsp";

//                    } else {
//
////                        LogError err = new LogError();
////                        err.setHaveError(true);
////                        err.setMessageid("0001");
////                        err.setMessage("Incorrect username or password!");
////                        err.setType("Danger");
////                        session.setAttribute("login_error", err);
//                        urlsend = "/login.jsp";
//
//                    }
//            
                RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                String urlerror = "/errorPage.jsp";
                RequestDispatcher dispatcher_error = getServletContext().getRequestDispatcher(urlerror);
                dispatcher_error.forward(request, response);
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (Exception ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
