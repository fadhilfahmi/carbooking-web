/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.smartbooking.controller;

import com.lcsb.smartbooking.dao.AccountingPeriod;
import com.lcsb.smartbooking.dao.BookingDAO;
import com.lcsb.smartbooking.dao.CarDAO;
import com.lcsb.smartbooking.dao.DriverDAO;
import com.lcsb.smartbooking.model.Book;
import com.lcsb.smartbooking.model.BookingDestinationTemp;
import com.lcsb.smartbooking.model.Car;
import com.lcsb.smartbooking.model.Driver;
import com.lcsb.smartbooking.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "PathController", urlPatterns = {"/PathController"})
public class PathController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String path = request.getParameter("path");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "addnewcar":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/conf_car_add.jsp";
                    break;
                case "bookcarstep":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/book_car_step.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp() + AccountingPeriod.getCurrentTime();
                    break;
                case "savecar":{
                    
                    Car c = new Car();
                    
                    c.setDescp(request.getParameter("descp"));
                    c.setPlateNo(request.getParameter("plateNo"));
                    c.setRemark(request.getParameter("remark"));
                    c.setStatus(request.getParameter("status"));
                    c.setDriverID(request.getParameter("driverID"));
                    
                    CarDAO.saveNewCar(log, c);
                    
                    urlsend = "/conf_car.jsp";
                    break;
                }
                case "updatecar":{
                    
                    Car c = new Car();
                    
                    c.setDescp(request.getParameter("descp"));
                    c.setPlateNo(request.getParameter("plateNo"));
                    c.setRemark(request.getParameter("remark"));
                    c.setStatus(request.getParameter("status"));
                    c.setDriverID(request.getParameter("driverID"));
                    c.setCarID(request.getParameter("carID"));
                    
                    CarDAO.updateCar(log, c);
                    
                    urlsend = "/conf_car.jsp";
                    break;
                }
                case "addmodal":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/conf_car_modal_view.jsp?carID="+request.getParameter("carID");
                    break;
                case "deletecar":
                    CarDAO.deleteCar(log, request.getParameter("carID"));
                    break;
                case "addnewdriver":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/conf_driver_add.jsp";
                    break;
                case "savedriver":{
                    
                    Driver c = new Driver();
                    
                    //c.setDriverID(request.getParameter("driverID"));
                    c.setDriverName(request.getParameter("driverName"));
                    c.setEmail(request.getParameter("email"));
                    c.setStaffID(request.getParameter("staffID"));
                    c.setStatus(request.getParameter("status"));
                    
                    DriverDAO.saveNewDriver(log, c);
                    
                    urlsend = "/conf_driver.jsp";
                    break;
                }
                case "updatedriver":{
                    
                    Driver c = new Driver();
                    
                    c.setDriverID(request.getParameter("driverID"));
                    c.setDriverName(request.getParameter("driverName"));
                    c.setEmail(request.getParameter("email"));
                    c.setStaffID(request.getParameter("staffID"));
                    c.setStatus(request.getParameter("status"));
                    
                    DriverDAO.updateDriver(log, c);
                    
                    urlsend = "/conf_driver.jsp";
                    break;
                }
                case "addmodaldriver":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/conf_driver_modal_view.jsp?driverID="+request.getParameter("driverID");
                    break;
                case "deletedriver":
                    CarDAO.deleteCar(log, request.getParameter("carID"));
                    break;
                case "savebook":{
                    
//                    Book c = new Book();
//                    
//                    c.setDestination(request.getParameter("destination"));
//                    c.setEnddate(request.getParameter("enddate"));
//                    c.setEndtime(request.getParameter("endtime"));
//                    c.setReason(request.getParameter("reason"));
//                    c.setStartdate(request.getParameter("startdate"));
//                    c.setStarttime(request.getParameter("starttime"));
//                    
//                    BookingDAO.saveNewBook(log, c);
                    
                    urlsend = "/conf_car.jsp";
                    break;
                }
                case "deletebooking":
                    BookingDAO.deleteBook(log, request.getParameter("bookID"));
                    break;
                case "assigncarmodal":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/book_car_assign_modal.jsp?bookID="+request.getParameter("bookID");
                    break;
                case "setdatefordestination":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/book_set_destination_date.jsp?id="+request.getParameter("id");
                    break;
                case "viewmodalpassenger":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/book_set_passenger_modal.jsp?sessionid="+request.getParameter("sessionid");
                    break;
                case "savebookmaster":
                    BookingDAO.saveBookingMaster(log, request.getParameter("sessionid"));
                    break;
                default:

                    //urlsend = "/error.jsp?moduleid=" + moduleid + "&refer=" + request.getParameter("referno");
                    break;
            }

            Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(PathController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(PathController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
