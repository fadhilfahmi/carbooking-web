<%-- 
    Document   : book_set_passenger_modal_view
    Created on : Nov 6, 2019, 3:36:19 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.dao.MemberDAO"%>
<%@page import="com.lcsb.smartbooking.model.Members"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>
<ul class="media-list media-list-divider scroller mr-2 pre-scrollable" data-height="470px">

    <%
        List<Members> listAll = (List<Members>) MemberDAO.getMember(log, request.getParameter("keyword"));

        for (Members j : listAll) {

    %>
    <li class="media thisresult-select" id="<%= j.getCompID() %>" title="<%= j.getMemberName() %>" href="<%= j.getImageURL() %>">
        <div class="media-body d-flex">
            <div class="flex-1">
                <div class="d-flex align-items-center font-13">
                    <img class="img-circle mr-2" src="<%= j.getImageURL() %>" alt="image" width="22" />
                    <a class="mr-2 text-success" href="javascript:;"><%= j.getMemberName() %></a>
                    <!--<span class="text-muted">1 hrs ago</span>-->
                </div>
            </div>
            <div class="text-right" style="width:100px;">
                <span class="badge badge-primary badge-pill mb-2">In Progress</span>

            </div>
        </div>
    </li>

    <%}
    %>
</ul>