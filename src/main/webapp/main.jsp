<%-- 
    Document   : main
    Created on : Sep 29, 2019, 9:59:49 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.dao.BookingDAO"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="row mb-4">
                    <div class="col-lg-4 col-md-6">
                        <div class="card mb-4">
                            <div class="card-body flexbox-b">
                                <div class="easypie mr-4" data-percent="73" data-bar-color="#18C5A9" data-size="80" data-line-width="8">
                                    <span class="easypie-data text-success" style="font-size:32px;"><i class="la la-users"></i></span>
                                </div>
                                <div>
                                    <h3 class="font-strong text-success"><%= BookingDAO.getNewBooking(log) %></h3>
                                    <div class="text-muted">New Booking</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card mb-4">
                            <div class="card-body flexbox-b">
                                <div class="easypie mr-4" data-percent="42" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                    <span class="easypie-data font-26 text-primary"><i class="ti-world"></i></span>
                                </div>
                                <div>
                                    <h3 class="font-strong text-primary">6400</h3>
                                    <div class="text-muted">TODAY'S VISITS</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card mb-4">
                            <div class="card-body flexbox-b">
                                <div class="easypie mr-4" data-percent="70" data-bar-color="#ff4081" data-size="80" data-line-width="8">
                                    <span class="easypie-data text-pink" style="font-size:32px;"><i class="la la-tags"></i></span>
                                </div>
                                <div>
                                    <h3 class="font-strong text-pink">210</h3>
                                    <div class="text-muted">SUPPORT TICKETS</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-7">
                        <div class="ibox">
                            <div class="ibox-body">
                                <div class="d-flex justify-content-between mb-4">
                                    <div>
                                        <h3 class="m-0">Drivers Analytics</h3>
                                        <div>See driver frequency</div>
                                    </div>
                                    <ul class="nav nav-pills nav-pills-rounded nav-pills-air" id="chart_tabs">
                                        <li class="nav-item ml-1">
                                            <a class="nav-link active" data-toggle="tab" data-id="1" href="javascript:;">This Week</a>
                                        </li>
                                        <li class="nav-item ml-1">
                                            <a class="nav-link" data-toggle="tab" data-id="2" href="javascript:;">Last Week</a>
                                        </li>
                                        <li class="nav-item ml-1">
                                            <a class="nav-link" data-toggle="tab" data-id="3" href="javascript:;">Last Year</a>
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <canvas id="visitors_chart" style="height:260px;"></canvas>
                                </div>
                            </div>
                            <hr>
                            <div class="ibox-body">
                                <div class="row">
                                    <div class="col-6 pl-4">
                                        <h6 class="mb-3">GENDER</h6>
                                        <span class="h2 mr-3"><i class="fa fa-male text-primary h1 mb-0 mr-2"></i>
                                            <span>56<sup>%</sup></span>
                                        </span>
                                        <span class="h2 mr-3"><i class="fa fa-female text-pink h1 mb-0 mr-2"></i>
                                            <span>32<sup>%</sup></span>
                                        </span>
                                        <span class="h2"><i class="fa fa-question text-light h1 mb-0 mr-2"></i>
                                            <span>12<sup>%</sup></span>
                                        </span>
                                    </div>
                                    <div class="col-6">
                                        <h6 class="mb-3">SCREENS</h6>
                                        <span class="h2 mr-3"><i class="ti-desktop text-primary mr-2"></i>
                                            <span>49<sup>%</sup></span>
                                        </span>
                                        <span class="h2 mr-3"><i class="ti-tablet text-pink mr-2"></i>
                                            <span>29<sup>%</sup></span>
                                        </span>
                                        <span class="h2"><i class="ti-mobile text-success mr-2"></i>
                                            <span>22<sup>%</sup></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="ibox ibox-fullheight">
                            <div class="ibox-head">
                                <div class="ibox-title">CAR USAGE</div>
                                <div class="ibox-tools">
                                    <a class="dropdown-toggle" data-toggle="dropdown"><i class="ti-more-alt"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item"><i class="la la-upload"></i>Export</a>
                                        <a class="dropdown-item"><i class="la la-file-excel-o"></i>Download</a>
                                        <a class="dropdown-item"><i class="la la-print"></i>Print</a>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-body">
                                <div class="mb-5">
                                    <div class="flexbox-b mb-2">
                                        <span class="badge-point badge-success mr-2"></span>20-27
                                        <span class="h4 mb-0 ml-4">25%</span>
                                    </div>
                                    <div class="flexbox-b mb-2">
                                        <span class="badge-point badge-primary mr-2"></span>35-50
                                        <span class="h4 mb-0 ml-4">22%</span>
                                    </div>
                                    <div class="flexbox-b mb-2">
                                        <span class="badge-point badge-pink mr-2"></span>27-35
                                        <span class="h4 mb-0 ml-4">17%</span>
                                    </div>
                                </div>
                                <div class="ibox-fullwidth-block">
                                    <canvas id="age_chart" style="height:260px;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-8">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="ibox ibox-fullheight">
                                    <div class="ibox-head">
                                        <div class="ibox-title">VISITORS SCREENS</div>
                                        <div class="ibox-tools">
                                            <a class="dropdown-toggle" data-toggle="dropdown"><i class="ti-desktop"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item"><i class="ti-pencil mr-2"></i>Create</a>
                                                <a class="dropdown-item"><i class="ti-pencil-alt mr-2"></i>Edit</a>
                                                <a class="dropdown-item"><i class="ti-close mr-2"></i>Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibox-body">
                                        <div>
                                            <canvas id="screens_chart" style="height:260px;"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="ibox ibox-fullheight">
                                    <div class="ibox-head">
                                        <div class="ibox-title">VISITORS BROWSERS</div>
                                    </div>
                                    <div class="ibox-body">
                                        <ul class="list-group list-group-divider list-group-full">
                                            <li class="list-group-item flexbox">
                                                <span class="flexbox"><i class="la la-chrome mr-3 font-40"></i>Google Chrome</span>
                                                <span class="badge badge-success badge-pill">34.7%</span>
                                            </li>
                                            <li class="list-group-item flexbox">
                                                <span class="flexbox"><i class="la la-firefox mr-3 font-40"></i>Mozila Firefox</span>
                                                <span class="badge badge-primary badge-pill">34.7%</span>
                                            </li>
                                            <li class="list-group-item flexbox">
                                                <span class="flexbox"><i class="la la-opera mr-3 font-40"></i>Opera</span>
                                                <span class="badge badge-pink badge-pill">34.7%</span>
                                            </li>
                                            <li class="list-group-item flexbox">
                                                <span class="flexbox"><i class="la la-internet-explorer mr-3 font-40"></i>Internet Explorer</span>
                                                <span class="badge badge-info badge-pill">34.7%</span>
                                            </li>
                                            <li class="list-group-item flexbox">
                                                <span class="flexbox"><i class="la la-safari mr-3 font-40"></i>Safari</span>
                                                <span class="badge badge-warning badge-pill">34.7%</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ibox">
                            <div class="ibox-body">
                                <div class="flexbox mb-5">
                                    <div class="flexbox">
                                        <span class="mr-4" style="width:60px;height:60px;background-color:#e7e9f6;color:#5c6bc0;display:inline-flex;align-items:center;justify-content:center;font-size:35px;"><i class="ti-files"></i></span>
                                        <div>
                                            <h5 class="font-strong">TOP 5 PAGES</h5>
                                            <div class="text-light">Top 5 visited pages on your site.</div>
                                        </div>
                                    </div>
                                    <ul class="nav nav-pills nav-pills-rounded nav-pills-air">
                                        <li class="nav-item ml-1">
                                            <a class="nav-link active" href="#tab_1" data-toggle="tab">This Week</a>
                                        </li>
                                        <li class="nav-item ml-1">
                                            <a class="nav-link" href="#tab_2" data-toggle="tab">Last Week</a>
                                        </li>
                                        <li class="nav-item ml-1">
                                            <a class="nav-link" href="#tab_3" data-toggle="tab">This Month</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="tab_1">
                                        <div class="flexbox">
                                            <div class="text-center">
                                                <div class="mb-3">Blog Catalog</div>
                                                <div class="easypie" data-percent="44" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">44<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Shop Catalog</div>
                                                <div class="easypie" data-percent="28" data-bar-color="#18c5a9" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">28<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Profile</div>
                                                <div class="easypie" data-percent="9" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">9<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Main Page</div>
                                                <div class="easypie" data-percent="13" data-bar-color="#ff4081" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">13<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Profile</div>
                                                <div class="easypie" data-percent="6" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">6<sup>%</sup></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_2">
                                        <div class="flexbox">
                                            <div class="text-center">
                                                <div class="mb-3">Blog Catalog</div>
                                                <div class="easypie" data-percent="33" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">33<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Shop Catalog</div>
                                                <div class="easypie" data-percent="42" data-bar-color="#18c5a9" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">42<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Profile</div>
                                                <div class="easypie" data-percent="8" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">8<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Main Page</div>
                                                <div class="easypie" data-percent="11" data-bar-color="#ff4081" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">11<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Profile</div>
                                                <div class="easypie" data-percent="6" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">6<sup>%</sup></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_3">
                                        <div class="flexbox">
                                            <div class="text-center">
                                                <div class="mb-3">Blog Catalog</div>
                                                <div class="easypie" data-percent="41" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">41<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Shop Catalog</div>
                                                <div class="easypie" data-percent="32" data-bar-color="#18c5a9" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">32<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Profile</div>
                                                <div class="easypie" data-percent="10" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">10<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Main Page</div>
                                                <div class="easypie" data-percent="12" data-bar-color="#ff4081" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">12<sup>%</sup></span>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="mb-3">Profile</div>
                                                <div class="easypie" data-percent="5" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                                    <span class="easypie-data h3 font-strong">5<sup>%</sup></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="ibox ibox-fullheight">
                            <div class="ibox-head">
                                <div class="ibox-title">NEW CUSTOMERS</div>
                                <div class="ibox-tools">
                                    <a class="dropdown-toggle" data-toggle="dropdown"><i class="ti-user"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item"><i class="ti-pencil mr-2"></i>Create</a>
                                        <a class="dropdown-item"><i class="ti-pencil-alt mr-2"></i>Edit</a>
                                        <a class="dropdown-item"><i class="ti-close mr-2"></i>Remove</a>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-body">
                                <ul class="media-list media-list-divider mr-2 scroller" data-height="580px">
                                    <li class="media align-items-center">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="./assets/img/users/u8.jpg" alt="image" width="54" />
                                        </a>
                                        <div class="media-body d-flex align-items-center">
                                            <div class="flex-1">
                                                <div class="media-heading">Lynn Weaver</div><small class="text-muted">Lorem Ipsum is simply dummy</small></div>
                                            <button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>
                                        </div>
                                    </li>
                                    <li class="media align-items-center">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="./assets/img/users/u6.jpg" alt="image" width="54" />
                                        </a>
                                        <div class="media-body d-flex align-items-center">
                                            <div class="flex-1">
                                                <div class="media-heading">Connor Perez</div><small class="text-muted">Lorem Ipsum is simply dummy</small></div>
                                            <button class="btn btn-sm btn-primary btn-rounded">Followed</button>
                                        </div>
                                    </li>
                                    <li class="media align-items-center">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="./assets/img/users/u11.jpg" alt="image" width="54" />
                                        </a>
                                        <div class="media-body d-flex align-items-center">
                                            <div class="flex-1">
                                                <div class="media-heading">Tyrone Carroll</div><small class="text-muted">Lorem Ipsum is simply dummy</small></div>
                                            <button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>
                                        </div>
                                    </li>
                                    <li class="media align-items-center">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="./assets/img/users/u10.jpg" alt="image" width="54" />
                                        </a>
                                        <div class="media-body d-flex align-items-center">
                                            <div class="flex-1">
                                                <div class="media-heading">Stella Obrien</div><small class="text-muted">Lorem Ipsum is simply dummy</small></div>
                                            <button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>
                                        </div>
                                    </li>
                                    <li class="media align-items-center">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="./assets/img/users/u2.jpg" alt="image" width="54" />
                                        </a>
                                        <div class="media-body d-flex align-items-center">
                                            <div class="flex-1">
                                                <div class="media-heading">Becky Brooks</div><small class="text-muted">Lorem Ipsum is simply dummy</small></div>
                                            <button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>
                                        </div>
                                    </li>
                                    <li class="media align-items-center">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="./assets/img/users/u5.jpg" alt="image" width="54" />
                                        </a>
                                        <div class="media-body d-flex align-items-center">
                                            <div class="flex-1">
                                                <div class="media-heading">Bob Gonzalez</div><small class="text-muted">Lorem Ipsum is simply dummy</small></div>
                                            <button class="btn btn-sm btn-primary btn-rounded">Followed</button>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
</body>
</html>
