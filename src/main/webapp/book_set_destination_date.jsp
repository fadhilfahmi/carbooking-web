<%-- 
    Document   : book_set_destination_date
    Created on : Nov 6, 2019, 11:38:53 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.model.BookingDestinationTemp"%>
<%@page import="com.lcsb.smartbooking.dao.BookingDAO"%>
<%@page import="com.lcsb.smartbooking.dao.DateAndTime"%>
<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page import="com.lcsb.smartbooking.model.Car"%>
<%@page import="com.lcsb.smartbooking.dao.CarDAO"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    BookingDestinationTemp b = (BookingDestinationTemp) BookingDAO.getBookingDestinationTempByID(log, request.getParameter("id"));

    if(b.getStartdate().equals("0000-00-00")) {
        b.setStartdate(DateAndTime.getCurrentTimeStamp());
        b.setEnddate(DateAndTime.getCurrentTimeStamp());
        b.setStarttime("00:00");
        b.setEndtime("00:00");
    }


%>

<script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="./assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="./assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="./assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="./assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $("#savedate").unbind('click').bind('click', function (e) {
//var a = $("form").serialize();

            var id = $('#id').val();
            var sd = $('#startdate').val();
            var ed = $('#enddate').val();
            var st = $('#starttime').val();
            var et = $('#endtime').val();

            var a = $("#modalcontent :input").serialize();
            console.log(a);
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "ProcessController?process=savedatetime",
                success: function (result) {
                    $('#myModal').modal('toggle')
                    console.log(id);
                    $('#datechange' + id).html('(From ' + sd + ' to ' + ed + ')');
                    $('#datebutton' + id).html('Change');
                }
            });
            //} 

            e.stopPropagation();
            return false;
        });


        $("#back").click(function () {
            parent.history.back();
            return false;
        });

        $('#date_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('.clockpicker').clockpicker();

        $(".select2_demo_2").select2({
            placeholder: "Select a state",
            allowClear: true
        });

    });

</script>
<div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="modalcontent">
            <input type="hidden" name="id" id="id" value="<%= b.getId()%>">
            <div class="modal-header p-4">
                <h5 class="modal-title"><%= b.getDesttype()%> - <%= b.getDestdescp()%></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">

                <div class="form-group" id="date_5">
                    <label class="font-normal">From and To Date</label>
                    <div class="input-daterange input-group" id="datepicker">
                        <input class="input-sm form-control" type="text" name="startdate" id="startdate" value="<%= b.getStartdate() %>">
                        <span class="input-group-addon pl-2 pr-2">to</span>
                        <input class="input-sm form-control" type="text" name="enddate" id="enddate" value="<%= b.getEnddate() %>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group mb-4">
                        <div class="form-group mb-4">
                            <label>Start Time (Optional)</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input class="form-control" type="text" value="<%= b.getStarttime() %>" name="starttime" id="starttime" >
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group mb-4">
                        <div class="form-group mb-4">
                            <label>End Time (Optional)</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input class="form-control" type="text" value="<%= b.getEndtime() %>" name="endtime" id="endtime">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                    <button class="btn btn-primary btn-rounded mr-3" id="savedate">Save</button>
                </div>

            </div>
        </form>
    </div>
</div>